use super::{with_scheduler, SwitchReason};
use crate::thread::ThreadId;
use alloc::boxed::Box;
use core::mem;
use core::raw::TraitObject;
use x86_64::VirtAddr;

/// Holds a Stack Pointer for a Thread
pub struct ThreadStack {
    pointer: VirtAddr,
}

impl ThreadStack {
    /// Creates a new `ThreadStack` with the given pointer
    ///
    /// Requires the stack to be allocated somewhere else, it will not
    /// allocate its own stack
    pub unsafe fn new(stack_pointer: VirtAddr) -> Self {
        ThreadStack {
            pointer: stack_pointer,
        }
    }

    pub fn get_stack_pointer(&self) -> VirtAddr {
        self.pointer
    }

    /// Sets up a Thread's stack to run a Rust closure
    pub fn with_closure(&mut self, closure: Box<dyn FnOnce() -> !>) {
        let closure_as_trait_object: TraitObject = unsafe { mem::transmute(closure) };

        // Push the closure onto the stack we're pointing to
        unsafe { self.push(closure_as_trait_object.data) };
        unsafe { self.push(closure_as_trait_object.vtable) };

        // Set the thread's entry point to a function that loads the
        // closure
        self.set_up_entry_point(entry_point_with_closure);
    }

    /// Set up the thread's entry point
    pub fn set_up_entry_point(&mut self, entry_point: fn() -> !) {
        unsafe { self.push(entry_point) };

        // Set the RFLAGS register in a way that ensures interrupts
        // will be enabled
        let rflags: u64 = 0x200;

        unsafe { self.push(rflags) };
    }

    /// Push a value onto the stack
    unsafe fn push<T>(&mut self, value: T) {
        // Decrement the pointer of the stack by the size of the data
        self.pointer -= core::mem::size_of::<T>();

        let data_target: *mut T = self.pointer.as_mut_ptr();

        data_target.write(value);
    }
}

pub unsafe fn context_switch_to(
    new_stack_pointer: VirtAddr,
    prev_thread_id: ThreadId,
    switch_reason: SwitchReason,
) {
    // This asm! invocation uses an interesting way to store the
    // registers before switching. It tells rust's compiler that
    // all registers must be preserved before it enters the asm so
    // rust does it for us, and when we return it will load all
    // the registers back on its own.

    llvm_asm!(
        "call asm_context_switch"
        :
        : "{rdi}"(new_stack_pointer), "{rsi}"(prev_thread_id), "{rdx}"(switch_reason as u64)
        : "rax", "rbx", "rcx", "rdx", "rsi", "rdi", "rbp", "r8", "r9", "r10",
        "r11", "r12", "r13", "r14", "r15", "rflags", "memory"
        : "intel", "volatile"
    );

    // This is where we end up when we context switch back to the
    // thread.
}

global_asm!(
    "
    .intel_syntax noprefix
    // Function Signature: asm_context_switch(stack_pointer: u64, thread_id: u64)
    asm_context_switch:
        pushfq

        // Set up arguments for function call
        mov rax, rsp
        mov rsp, rdi
        mov rdi, rax
        call add_paused_thread
        
        popfq
        ret
"
);

#[no_mangle]
pub extern "C" fn add_paused_thread(
    paused_stack_pointer: VirtAddr,
    paused_thread_id: ThreadId,
    switch_reason: SwitchReason,
) {
    with_scheduler(|s| s.add_paused_thread(paused_stack_pointer, paused_thread_id, switch_reason));
}

#[naked]
fn entry_point_with_closure() -> ! {
    unsafe {
        llvm_asm!("
        // Load the data and vtable we pushed onto the stack into
        // the registers used for the function call
        pop rsi
        pop rdi
        call call_closure
    " ::: "mem" : "intel", "volatile")
    };
    unreachable!();
}

// no_mangle required because of https://github.com/rust-lang/rust/issues/68136
#[no_mangle]
extern "C" fn call_closure(data: *mut (), vtable: *mut ()) -> ! {
    // Assemble the trait object
    let raw_trait_object = TraitObject { data, vtable };
    // Convert the trait object to a closure
    let closure: Box<dyn FnOnce() -> !> = unsafe { mem::transmute(raw_trait_object) };
    closure()
}
