#![no_std]
#![feature(llvm_asm)]
#![feature(global_asm)]
#![feature(raw)]
#![feature(option_expect_none)]
#![feature(never_type)]
#![feature(naked_functions)]

pub mod context_switch;
pub mod scheduler;
pub mod thread;

extern crate alloc;

use driver_vga::vga_println;
use scheduler::Scheduler;

static SCHEDULER: spin::Mutex<Option<Scheduler>> = spin::Mutex::new(None);

#[repr(u64)]
pub enum SwitchReason {
    Paused,
    Yield,
    Blocked,
    Sleep,
    Exit,
}

/// Try to switch to the next scheduled thread
pub fn invoke_scheduler() {
    let next_thread = SCHEDULER
        .try_lock()
        .and_then(|mut scheduler| scheduler.as_mut().and_then(|s| s.schedule()));
    if let Some((next_stack_pointer, prev_thread_id)) = next_thread {
        unsafe {
            context_switch::context_switch_to(
                next_stack_pointer,
                prev_thread_id,
                SwitchReason::Paused,
            )
        };
    }
}

/// Attempts to end the thread
pub fn suicide() -> ! {
    synchronous_context_switch(SwitchReason::Exit).expect("Can't exit last running thread!");
    unreachable!("Finished thread continued.");
}

/// Manually yields processor control
pub fn yield_now() {
    let _ = synchronous_context_switch(SwitchReason::Yield);
}

pub fn synchronous_context_switch(reason: SwitchReason) -> Result<(), ()> {
    let next = with_scheduler(|s| s.schedule());

    match next {
        Some((next_stack_pointer, prev_thread_id)) => unsafe {
            context_switch::context_switch_to(next_stack_pointer, prev_thread_id, reason);
            Ok(())
        },
        None => Err(()),
    }
}

pub fn with_scheduler<F, T>(f: F) -> T
where
    F: FnOnce(&mut Scheduler) -> T,
{
    x86_64::instructions::interrupts::without_interrupts(|| {
        f(SCHEDULER.lock().get_or_insert_with(Scheduler::new))
    })
}

/// Maybe sleeps the thread, maybe not. Nobody knows
pub fn sleep(arb_number: u64) {
    with_scheduler(|scheduler| {
        let thread_id = scheduler.current_thread_id();

        scheduler.add_sleeping_thread(thread_id, arb_number);
    });

    synchronous_context_switch(SwitchReason::Sleep).expect("Unable to sleep thread");
}

pub fn export_plug(
    plug: oxide_plug::PlugDefinition,
    implementation: oxide_plug::PlugImpl,
) -> Result<(), ()> {
    let id = oxide_plug::with_loaded_plugs(|loaded_plugins| {
        let insert_result = loaded_plugins.insert_plug_definition(plug);
        vga_println!("Insert result: {:?}", insert_result);
        insert_result.unwrap()
    });

    let thread_id = with_scheduler(|scheduler| {
        let thread = scheduler
            .get_thread(scheduler.current_thread_id())
            .expect("Failed to get thread");

        thread.plugs.insert(id, implementation);

        scheduler.current_thread_id()
    });

    let provider_insert = oxide_plug::with_plug_providers(|providers| {
        providers.register_plug_provider(thread_id.as_u64(), id)
    });

    vga_println!("Provider insert result: {:?}", provider_insert);

    Ok(())
}

pub fn export_static_plug(
    plug: oxide_plug::PlugDefinition,
    implementation: oxide_plug::PlugImpl,
) -> Result<(), ()> {
    let id = oxide_plug::with_loaded_plugs(|loaded_plugins| {
        let insert_result = loaded_plugins.insert_plug_definition(plug);
        crate::vga_println!("Insert result: {:?}", insert_result);
        insert_result.unwrap()
    });

    let thread_id = with_scheduler(|scheduler| {
        let thread = scheduler
            .get_thread(thread::ThreadId(0))
            .expect("Failed to get thread");

        thread.plugs.insert(id, implementation);

        thread::ThreadId(0)
    });

    let provider_insert = oxide_plug::with_plug_providers(|providers| {
        providers.register_plug_provider(thread_id.as_u64(), id)
    });

    crate::vga_println!("Provider insert result: {:?}", provider_insert);

    Ok(())
}
