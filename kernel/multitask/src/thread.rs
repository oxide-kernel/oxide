// use oxide_core::hlt_loop;
use crate::context_switch::ThreadStack;
use crate::scheduler::Scheduler;
use oxide_memory::stack::{alloc_stack, StackBounds};

use oxide_plug::PlugImpl;

use alloc::boxed::Box;
use alloc::collections::btree_map::BTreeMap;
use core::sync::atomic::{AtomicU64, Ordering};
use x86_64::{
    structures::paging::{mapper, FrameAllocator, Mapper, Size4KiB},
    VirtAddr,
};

#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct ThreadId(pub u64);

impl ThreadId {
    pub fn as_u64(&self) -> u64 {
        self.0
    }

    fn new() -> Self {
        static NEXT_THREAD_ID: AtomicU64 = AtomicU64::new(1);
        ThreadId(NEXT_THREAD_ID.fetch_add(1, Ordering::SeqCst))
    }
}

#[derive(Debug)]
pub struct Thread {
    id: ThreadId,
    pub plugs: BTreeMap<u64, PlugImpl>,
    stack_pointer: Option<VirtAddr>,
    stack_bounds: Option<StackBounds>,
}

impl Thread {
    pub fn create(
        entry_point: fn() -> !,
        stack_size: u64,
        mapper: &mut impl Mapper<Size4KiB>,
        frame_allocator: &mut impl FrameAllocator<Size4KiB>,
    ) -> Result<Self, mapper::MapToError<Size4KiB>> {
        let stack_bounds = alloc_stack(stack_size, mapper, frame_allocator)?;
        let mut stack = unsafe { ThreadStack::new(stack_bounds.end()) };
        stack.set_up_entry_point(entry_point);
        Ok(Self::new(stack.get_stack_pointer(), stack_bounds))
    }

    pub fn create_from_closure<F>(
        closure: F,
        stack_size: u64,
        mapper: &mut impl Mapper<Size4KiB>,
        frame_allocator: &mut impl FrameAllocator<Size4KiB>,
    ) -> Result<Self, mapper::MapToError<Size4KiB>>
    where
        F: FnOnce() -> ! + 'static + Send + Sync,
    {
        let stack_bounds = alloc_stack(stack_size, mapper, frame_allocator)?;
        let mut stack = unsafe { ThreadStack::new(stack_bounds.end()) };
        stack.with_closure(Box::new(closure));
        Ok(Self::new(stack.get_stack_pointer(), stack_bounds))
    }

    fn new(stack_pointer: VirtAddr, stack_bounds: StackBounds) -> Self {
        Thread {
            id: ThreadId::new(),
            plugs: BTreeMap::new(),
            stack_pointer: Some(stack_pointer),
            stack_bounds: Some(stack_bounds),
        }
    }

    pub(super) fn create_root_thread() -> Self {
        Thread {
            id: ThreadId(0),
            plugs: BTreeMap::new(),
            stack_pointer: None,
            stack_bounds: None,
        }
    }

    pub fn id(&self) -> ThreadId {
        self.id
    }

    pub(super) fn stack_pointer(&mut self) -> &mut Option<VirtAddr> {
        &mut self.stack_pointer
    }

    pub(super) fn stack_size(&self) -> Option<StackBounds> {
        self.stack_bounds
    }
}

pub trait ThreadIdExt {
    /// Tells the scheduler to kill the thread
    fn suicide(self, scheduler: &mut Scheduler) -> !;
}

impl ThreadIdExt for ThreadId {
    fn suicide(self, scheduler: &mut Scheduler) -> ! {
        scheduler.kill_thread(self);
        loop {}
        //hlt_loop()
    }
}
