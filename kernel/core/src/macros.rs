#[macro_export]
macro_rules! kprint {
    ($($arg:tt)*) => ($crate::macros::_kprint(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! kprintln {
    () => ($crate::kprint!("\n"));
    ($($arg:tt)*) => ($crate::kprint!("{}\n", format_args!($($arg)*)));
}

#[macro_export]
macro_rules! kerr {
    ($($arg:tt)*) => ($crate::macros::_kprint(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! kerrln {
    () => ($crate::kprint!("\n"));
    ($($arg:tt)*) => ($crate::kprint!("{}\n", format_args!($($arg)*)));
}

pub fn _kprint(args: core::fmt::Arguments) {
    use core::fmt::Write;

    let plug_id = oxide_plug::with_loaded_plugs(|loaded_plugs| {
        loaded_plugs.try_get_plug_id(kplug::PLUG_KPRINT)
    })
    .unwrap();

    let mut output = alloc::string::String::new();

    core::fmt::write(&mut output, args).unwrap();

    crate::call_plug(plug_id, output.as_bytes());
}
