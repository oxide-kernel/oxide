#![no_std]
#![no_main]

mod kinit;

use bootloader::{entry_point, BootInfo};

use oxide_multitask::thread::Thread;
use oxide_multitask::{suicide, with_scheduler, yield_now};

use oxide_core::{call_plug, kprintln, with_mapper_and_allocator};

use driver_kmutex::interface::KMutex;

use oxide_plug::with_loaded_plugs;
extern crate alloc;

use alloc::sync::Arc;

use kinit::kinit;

// Set the entry point for the bootloader
entry_point!(kmain);

/// The main function of the kernel. This is the entry point which gets
/// loaded on boot.
fn kmain(boot_info: &'static BootInfo) -> ! {
    x86_64::instructions::interrupts::without_interrupts(|| kinit(boot_info));
    // x86_64::instructions::interrupts::disable();

    with_scheduler(|scheduler| {
        let test_thread = with_mapper_and_allocator(|mapper, frame_allocator| {
            Thread::create_from_closure(
                || {
                    kprintln!("[THREAD 1] Creating Mutex");
                    let mutex_id = Arc::new(KMutex::new(5));
                    oxide_core::kprintln!("[THREAD 1] Created Mutex ID: {}", -1);
                    kprintln!("[THREAD 1] Locking Mutex");
                    let lock = mutex_id.lock();

                    let mutex_clone = mutex_id.clone();

                    with_scheduler(|scheduler| {
                        let other_thread = with_mapper_and_allocator(|mapper, frame_allocator| {
                            Thread::create_from_closure(
                                move || {
                                    driver_vga::vga_println!("[THREAD 2] Hi!");

                                    driver_vga::vga_println!("[THREAD 2] Locking Mutex");
                                    mutex_clone.lock();

                                    kprintln!("[THREAD 2] Yay!");

                                    suicide()
                                },
                                0x10,
                                mapper,
                                frame_allocator,
                            )
                            .unwrap()
                        });
                        scheduler.add_new_thread(other_thread)
                    });

                    yield_now();

                    driver_vga::vga_println!(
                        "[THREAD 1] Sleeping for some magical amount of time."
                    );

                    // Abpout 10 numbers per second
                    oxide_multitask::sleep(40);

                    driver_vga::vga_println!("[THREAD 1] Unlocking");
                    drop(lock);

                    driver_vga::vga_println!("[THREAD 1] Unlocked");

                    suicide()
                },
                0x10,
                mapper,
                frame_allocator,
            )
            .unwrap()
        });

        scheduler.add_new_thread(test_thread)
    });

    loop {
        yield_now();
        x86_64::instructions::interrupts::enable_interrupts_and_hlt()
    }
}
