#![no_std]
#![feature(alloc_error_handler)]

pub mod macros;

extern crate alloc;

use core::panic::PanicInfo;
use driver_kmutex::driver::{KMutex, KMutexRef};
use driver_vga::vga_println;
use kplug::{PLUG_KMUTEX_CREATE, PLUG_KMUTEX_LOCK, PLUG_KMUTEX_UNLOCK};

use arch::hlt_loop;

/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    vga_println!("{}", info);
    hlt_loop()
}

// Define the global allocation error handler for the Kernel
#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}

use oxide_multitask::thread::ThreadId;
use oxide_multitask::with_scheduler;
use oxide_plug::{with_loaded_plugs, with_plug_providers};

pub fn call_plug(plug: u64, data: &[u8]) -> u64 {
    let func = with_scheduler(|scheduler| {
        let provider_thread_id = with_plug_providers(|plug_providers| {
            // This is not safe
            *plug_providers
                .get_plug_providers(plug)
                .unwrap()
                .first()
                .unwrap()
        });

        // Get the `Thread` that the plug is provided on
        let provider_thread = scheduler
            .get_thread(ThreadId(provider_thread_id))
            .expect("Failed to get provider thread");

        // Get the provider closure and call it
        let plug_func = &provider_thread
            .plugs
            .get(&plug)
            .expect("Failed to get plug implementation")
            .plug as *const dyn Fn(&[u8]) -> u64;

        plug_func
    });

    let func_deref = unsafe { &*func };

    func_deref(data)
}

use driver_vga::WRITER;
use kplug::PLUG_KPRINT;
use oxide_multitask::thread::Thread;
use oxide_multitask::{export_static_plug, yield_now};
use oxide_plug::PlugImpl;

use alloc::boxed::Box;
use core::str::from_utf8;
use x86_64::instructions::interrupts;

pub fn threading_init() {
    fn idle_thread_func() -> ! {
        loop {
            x86_64::instructions::hlt();
            yield_now();
        }
    }
    let idle_thread = with_mapper_and_allocator(|mapper, frame_allocator| {
        Thread::create(idle_thread_func, 2, mapper, frame_allocator)
            .expect("Failed to create idle thread")
    });

    with_scheduler(|scheduler| scheduler.set_idle_thread(idle_thread));
}

pub fn with_mapper_and_allocator<F, T>(f: F) -> T
where
    F: FnOnce(
        &mut x86_64::structures::paging::OffsetPageTable,
        &mut oxide_memory::frame_allocator_boot_info::FrameAllocatorBootInfo,
    ) -> T,
{
    x86_64::instructions::interrupts::without_interrupts(|| {
        let mut mapper_lock = oxide_memory::MAPPER.lock();
        let mapper = mapper_lock.as_mut().unwrap();
        let mut frame_allocator_lock = oxide_memory::FRAME_ALLOCATOR.lock();
        let frame_allocator = frame_allocator_lock.as_mut().unwrap();

        f(mapper, frame_allocator)
    })
}

pub fn load_kernel_plugs() {
    // Register `PLUG_KPRINT`
    let plug_kprint = PlugImpl {
        plug: Box::new(|utf8_buffer| {
            interrupts::without_interrupts(|| {
                WRITER.lock().write_string(from_utf8(utf8_buffer).unwrap());
            });

            0
        }),
    };

    export_static_plug(PLUG_KPRINT, plug_kprint).expect("Failed to register PLUG_KPRINT");

    // Register KMutex plugs
    let plug_kmutex_create = PlugImpl {
        plug: Box::new(|_| KMutex::new().0),
    };

    export_static_plug(PLUG_KMUTEX_CREATE, plug_kmutex_create)
        .expect("Failed to register PLUG_KMUTEX_CREATE");

    let plug_kmutex_lock = PlugImpl {
        plug: Box::new(|args| {
            // Screw the rust gods
            let mutex_id = args.as_ptr() as *const u64;

            let write_into = (args.as_ptr() as usize + 4_usize) as *const u64;

            KMutex::lock(&KMutexRef(unsafe { *mutex_id }));

            0
        }),
    };

    export_static_plug(PLUG_KMUTEX_LOCK, plug_kmutex_lock)
        .expect("Failed to register PLUG_KMUTEX_LOCK");

    let plug_kmutex_free = PlugImpl {
        plug: Box::new(|args| {
            // Screw the rust gods
            let mutex_id = args.as_ptr() as *const u64;

            KMutex::free(&KMutexRef(unsafe { *mutex_id }));

            0
        }),
    };

    export_static_plug(PLUG_KMUTEX_UNLOCK, plug_kmutex_free)
        .expect("Failed to register PLUG_KMUTEX_FREE");
}
