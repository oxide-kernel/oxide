use bootloader::BootInfo;

use oxide_memory::allocator::init_heap;
use oxide_memory::{init_allocator, FRAME_ALLOCATOR, MAPPER};

use oxide_plug::init_tables;

use driver_vga::vga_println;

use oxide_core::with_mapper_and_allocator;
use oxide_core::{kprintln, load_kernel_plugs, threading_init};

use arch::cpu_init;

/// initializes the Kernel.
pub fn kinit(boot_info: &'static BootInfo) {
    // TODO: Set the logging plug to print to VGA

    vga_println!("Stage 1 | CPU Initialization");

    cpu_init();

    vga_println!("[COMPLETE]\n");

    vga_println!("Stage 2 | Kernel Structure Initialization");

    vga_println!("Initializing Paging Allocator");
    init_allocator(boot_info);

    vga_println!("Initializing Kernel Heap");

    with_mapper_and_allocator(|mapper, frame_allocator| {
        init_heap(mapper, frame_allocator).expect("Heap init Failed")
    });

    vga_println!("Initializing Plug Tables");
    init_tables();

    vga_println!("Initializing Threading");
    threading_init();

    vga_println!("Loading Kernel Plugs");
    load_kernel_plugs();

    // Kernel plugs have been loaded so its safe to use `kprintln`

    kprintln!("[COMPLETE]\n");

    kprintln!("Oxide Kernel Initialization Complete");
}
