#![no_std]

pub mod allocator;
pub mod frame_allocator_boot_info;
pub mod paging;
pub mod stack;

use bootloader::BootInfo;
use frame_allocator_boot_info::FrameAllocatorBootInfo;
use lazy_static::lazy_static;
use spin::Mutex;
use x86_64::structures::paging::mapper::OffsetPageTable;
use x86_64::VirtAddr;

// Create global allocator and mapper
//
// Used in the rest of the kernel to make allocations
lazy_static! {
    pub static ref MAPPER: Mutex<Option<OffsetPageTable<'static>>> = Mutex::new(None);
    pub static ref FRAME_ALLOCATOR: Mutex<Option<FrameAllocatorBootInfo>> = Mutex::new(None);
}

pub fn init_allocator(boot_info: &'static BootInfo) {
    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);

    let mapper: OffsetPageTable<'static> = unsafe { paging::init(phys_mem_offset) };

    let frame_allocator = unsafe { FrameAllocatorBootInfo::init(&boot_info.memory_map) };

    // Set the global kernel mapper
    let mut mapper_static = MAPPER.lock();
    *mapper_static = Some(mapper);

    // Set the global frame allocator
    let mut frame_allocator_static = FRAME_ALLOCATOR.lock();
    *frame_allocator_static = Some(frame_allocator);
}
