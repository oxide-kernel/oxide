#![no_std]

use oxide_plug::PlugDefinition;

/// kprint(string_buffer: [u8])
///
/// Prints a utf-8 string to the Kernel's STDOUT
pub const PLUG_KPRINT: PlugDefinition = PlugDefinition {
    identifier: "kprintln",
    argument_count: 1,
    argument_size: 8,
};

/// kbuff_create() -> u64
///
/// Creates a new buffer and returns it.
pub const PLUG_KBUFF_CREATE: PlugDefinition = PlugDefinition {
    identifier: "kbuff_create",
    argument_count: 0,
    argument_size: 0,
};

/// kbuff_destroy(buffer: u64)
///
/// Destroys a buffer
pub const PLUG_KBUFF_DESTROY: PlugDefinition = PlugDefinition {
    identifier: "kbuff_destroy",
    argument_count: 1,
    argument_size: 4,
};

/// kbuff_write(buffer: u64, data: [u8])
///
/// Writes `data` into buffer `buffer`
pub const PLUG_KBUFF_WRITE: PlugDefinition = PlugDefinition {
    identifier: "kbuff_write",
    argument_count: 2,
    argument_size: 12,
};

/// kbuff_read(buffer: u64, size: u64, target: *const u8) -> u64
///
/// Reads `size` bytes or less from buffer `buffer`. Returns the
/// amount of bytes read.
pub const PLUG_KBUFF_READ: PlugDefinition = PlugDefinition {
    identifier: "kbuff_read",
    argument_count: 3,
    argument_size: 12,
};

/// kmutex_create() -> u64
///
/// Creates a mutex and returns its ID.
pub const PLUG_KMUTEX_CREATE: PlugDefinition = PlugDefinition {
    identifier: "kmutex_create",
    argument_count: 0,
    argument_size: 0,
};

/// kmutex_lock(mutex: u64, lock_result: *const Result<bool, ()>)
///
/// Blocks the thread until a lock can be aquired on the mutex.
pub const PLUG_KMUTEX_LOCK: PlugDefinition = PlugDefinition {
    identifier: "kmutex_lock",
    argument_count: 2,
    argument_size: 8,
};

/// kmutex_unlock(mutex: u64)
///
/// Unlocks the Mutex
pub const PLUG_KMUTEX_UNLOCK: PlugDefinition = PlugDefinition {
    identifier: "kmutex_unlock",
    argument_count: 1,
    argument_size: 4,
};
