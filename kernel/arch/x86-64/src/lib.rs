#![no_std]
#![feature(abi_x86_interrupt)]
#![feature(never_type)]

pub mod gdt;
pub mod interrupts;

use driver_vga::vga_println;

pub fn cpu_init() {
    vga_println!("Initializing GDT");
    gdt::init();

    vga_println!("Initializing IDT");
    interrupts::init_idt();

    vga_println!("Enabling Interrupts");
    unsafe { interrupts::PICS.lock().initialize() };
    //x86_64::instructions::interrupts::enable();
}

pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}
