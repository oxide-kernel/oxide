#![no_std]
#![feature(option_expect_none)]

extern crate alloc;

mod loaded_plugs;
mod plug_implementor;
mod plug_providers;

pub use loaded_plugs::{with_loaded_plugs, LoadedPlugs, PlugDefinition};
pub use plug_implementor::PlugImpl;
pub use plug_providers::{with_plug_providers, PlugProviders};

pub fn init_tables() {
    loaded_plugs::LOADED_PLUGS
        .lock()
        .get_or_insert_with(LoadedPlugs::new);
    plug_providers::PLUG_PROVIDERS
        .lock()
        .get_or_insert_with(PlugProviders::new);
}
