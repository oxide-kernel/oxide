use alloc::collections::btree_map::BTreeMap;
use core::sync::atomic::{AtomicU64, Ordering};

use spin::Mutex;

// Tracks the ID that will be assigned to the next Plug
pub(crate) static PLUG_ID: AtomicU64 = AtomicU64::new(0);

// Contains the loaded plugs
pub(crate) static LOADED_PLUGS: Mutex<Option<LoadedPlugs>> = Mutex::new(None);

type Result = core::result::Result<u64, LoadedPlugsError>;

#[derive(Debug)]
pub enum LoadedPlugsError {
    AlreadyLoaded(u64),
    NotLoaded,
}

/// Defines an interface of a plug
#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct PlugDefinition {
    pub identifier: &'static str,
    pub argument_count: u8,
    pub argument_size: u64,
}

#[derive(Debug)]
pub struct LoadedPlugs {
    /// Maps a plug layout to an ID
    plugs: BTreeMap<PlugDefinition, u64>,
}

// TODO
// Connect threads so that we can keep track of what threads care about a
// plug definition, this will allow us to free them.

impl LoadedPlugs {
    /// Create a new, empty, loaded plugs table.
    pub(crate) fn new() -> LoadedPlugs {
        LoadedPlugs {
            plugs: BTreeMap::new(),
        }
    }

    /// Tries to insert a new Plug definition.
    ///
    /// If that definition already exists, it will return an error.
    ///
    /// # TODO
    /// In the future we want to be able to control priority of plugs.
    pub fn insert_plug_definition(&mut self, plug: PlugDefinition) -> Result {
        if let Some(id) = self.plugs.get(&plug) {
            Err(LoadedPlugsError::AlreadyLoaded(*id))
        } else {
            let id = PLUG_ID.fetch_add(1, Ordering::Relaxed);
            self.plugs
                .insert(plug, id)
                .expect_none("Overwrote other plug definition");

            Ok(id)
        }
    }

    /// Tries to get the ID of a passed plug definition.
    pub fn try_get_plug_id(&self, definition: PlugDefinition) -> Result {
        if let Some(id) = self.plugs.get(&definition) {
            Ok(*id)
        } else {
            Err(LoadedPlugsError::NotLoaded)
        }
    }
}

/// Run a closure with mutable access to the currently loaded plugs
pub fn with_loaded_plugs<F, T>(f: F) -> T
where
    F: FnOnce(&mut LoadedPlugs) -> T,
{
    x86_64::instructions::interrupts::without_interrupts(|| {
        f(LOADED_PLUGS.lock().get_or_insert_with(LoadedPlugs::new))
    })
}
