use alloc::boxed::Box;

pub struct PlugImpl {
    pub plug: Box<dyn Fn(&[u8]) -> u64 + Send + 'static + Sync>,
}

impl core::fmt::Debug for PlugImpl {
    fn fmt(&self, fmt: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        fmt.write_str("PlugImpl")
    }
}
