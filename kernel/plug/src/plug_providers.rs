use alloc::collections::btree_map::BTreeMap;
use alloc::vec::Vec;

use spin::Mutex;

pub(crate) static PLUG_PROVIDERS: Mutex<Option<PlugProviders>> = Mutex::new(None);

type Result<T> = core::result::Result<T, PlugProviderError>;

#[derive(Debug)]
pub enum PlugProviderError {
    PlugNotRegistered,
    ThreadNotProvider,
    NoProviders,
}

#[derive(Debug)]
pub struct PlugProviders {
    // Maps plug IDs to thread ID
    pub providers: BTreeMap<u64, Vec<u64>>,
}

impl PlugProviders {
    pub(crate) fn new() -> PlugProviders {
        PlugProviders {
            providers: BTreeMap::new(),
        }
    }

    /// Register a thread as a provider for a thread
    pub fn register_plug_provider(&mut self, thread: u64, plug_id: u64) -> Result<()> {
        let plug_providers = match self.providers.get_mut(&plug_id) {
            Some(providers) => providers,
            None => {
                self.providers.insert(plug_id, Vec::new());

                self.providers
                    .get_mut(&plug_id)
                    .expect("Failed to get providers Vec.")
            }
        };

        plug_providers.push(thread);

        Ok(())
    }

    /// De-Register a plug from a thread
    pub fn deregister_plug_provider(&mut self, thread: u64, plug_id: u64) -> Result<()> {
        let plug_providers = match self.providers.get_mut(&plug_id) {
            Some(providers) => providers,
            None => return Err(PlugProviderError::ThreadNotProvider),
        };

        // Remove the plug ID from the vec
        plug_providers.retain(|item| *item != thread);

        if plug_providers.is_empty() {
            self.providers.remove(&plug_id);
        }

        Ok(())
    }

    /// Returns all threads that provide plug `plug_id`
    pub fn get_plug_providers(&self, plug_id: u64) -> Result<&Vec<u64>> {
        match self.providers.get(&plug_id) {
            Some(providers) => return Ok(&providers),
            None => return Err(PlugProviderError::NoProviders),
        };
    }
}

/// Run a closure with mutable access to the plug providers table
pub fn with_plug_providers<F, T>(f: F) -> T
where
    F: FnOnce(&mut PlugProviders) -> T,
{
    x86_64::instructions::interrupts::without_interrupts(|| {
        f(PLUG_PROVIDERS.lock().get_or_insert_with(PlugProviders::new))
    })
}
