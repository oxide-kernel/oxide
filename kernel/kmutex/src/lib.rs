#![no_std]

extern crate alloc;

use core::cell::UnsafeCell;

use driver_kmutex::driver::KMutexRef;
use kplug::{PLUG_KMUTEX_CREATE, PLUG_KMUTEX_LOCK, PLUG_KMUTEX_UNLOCK};

use alloc::boxed::Box;

use core::ops::{Deref, DerefMut};

pub struct KMutex<T> {
    raw: KMutexRef,
    inner: UnsafeCell<T>,
}

impl<T> KMutex<T> {
    pub fn new(inner: T) -> KMutex<T> {
        todo!()
    }

    pub fn lock(self) -> KMutexGuard<T> {
        KMutexGuard { inner: self }
    }

    pub fn lock_no_interrupt(&mut self) -> KMutexGuard<T> {
        todo!()
    }
}

pub struct KMutexGuard<T> {
    inner: KMutex<T>,
}

impl<T> Deref for KMutexGuard<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.inner.inner.get() }
    }
}

impl<T> DerefMut for KMutexGuard<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.inner.inner.get() }
    }
}

impl<T> Drop for KMutexGuard<T> {
    fn drop(&mut self) {
        // Free the Mutex with the OS
    }
}
