#![no_std]

extern crate alloc;

use core::cell::UnsafeCell;

use super::driver::KMutex as KMutexDriver;
use super::driver::KMutexRef;

use core::ops::{Deref, DerefMut};

pub struct KMutex<T> {
    raw: KMutexRef,
    inner: UnsafeCell<T>,
}

impl<T> KMutex<T> {
    pub fn new(inner: T) -> KMutex<T> {
        let mutex_ref = KMutexDriver::new();

        KMutex {
            raw: mutex_ref,
            inner: UnsafeCell::from(inner),
        }
    }

    pub fn lock(&self) -> KMutexGuard<T> {
        KMutexDriver::lock(&self.raw).unwrap();

        KMutexGuard { inner: self }
    }

    pub fn lock_no_interrupt(&mut self) -> KMutexGuard<T> {
        todo!()
    }
}

unsafe impl<T> Sync for KMutex<T> {}

pub struct KMutexGuard<'a, T: 'a> {
    inner: &'a KMutex<T>,
}

impl<T> Deref for KMutexGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.inner.inner.get() }
    }
}

impl<T> DerefMut for KMutexGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.inner.inner.get() }
    }
}

impl<T> Drop for KMutexGuard<'_, T> {
    fn drop(&mut self) {
        KMutexDriver::free(&self.inner.raw)
    }
}
