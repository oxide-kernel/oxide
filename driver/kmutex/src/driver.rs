use alloc::collections::{BTreeMap, VecDeque};
use core::sync::atomic::{AtomicBool, AtomicU64, Ordering};
use lazy_static::lazy_static;

use oxide_multitask::thread::ThreadId;
use oxide_multitask::with_scheduler;
use spin::Mutex;

lazy_static! {
    pub static ref MUTEX_TABLE: Mutex<BTreeMap<u64, KMutex>> = Mutex::new(BTreeMap::new());
}

#[derive(Copy, Clone, Default)]
#[repr(transparent)]
pub struct KMutexRef(pub u64);

pub struct KMutex {
    locked: AtomicBool,
    waiters: VecDeque<ThreadId>,
}

impl KMutex {
    pub fn new() -> KMutexRef {
        let mut mutex_table = MUTEX_TABLE.lock();
        static NEXT_MUTEX_ID: AtomicU64 = AtomicU64::new(1);
        let mutex_id = NEXT_MUTEX_ID.fetch_add(1, Ordering::SeqCst);

        let mutex = KMutex {
            locked: AtomicBool::new(false),
            waiters: VecDeque::new(),
        };

        mutex_table.insert(mutex_id, mutex);

        KMutexRef(mutex_id)
    }

    /// Blocks the thread until the mutex is locked
    pub fn lock(mutex: &KMutexRef) -> Result<(), ()> {
        let locked_notpog = Self::try_lock(mutex)?;

        drop(locked_notpog);

        let locked = locked_notpog.clone();

        if !locked {
            let mut mutex_table = MUTEX_TABLE.lock();

            let mutex = mutex_table.get_mut(&mutex.0).ok_or(())?;

            let thread_id = with_scheduler(|scheduler| scheduler.current_thread_id());

            mutex.waiters.push_back(thread_id);

            // Block this thread

            drop(mutex_table);

            oxide_multitask::synchronous_context_switch(oxide_multitask::SwitchReason::Blocked)
                .unwrap();

            // We have aquired the mutex

            Ok(())
        } else {
            Ok(())
        }
    }

    /// Tries to lock the Mutex, returns a result
    pub fn try_lock(mutex: &KMutexRef) -> Result<bool, ()> {
        let mut mutex_table = MUTEX_TABLE.lock();

        let mutex = mutex_table.get_mut(&mutex.0);

        if let Some(mutex) = mutex {
            let is_locked = mutex.locked.compare_and_swap(false, true, Ordering::SeqCst);

            if is_locked {
                // Return false

                Ok(false)
            } else {
                // We aquired the mutex, yay!
                Ok(true)
            }
        } else {
            Err(())
        }
    }

    // Frees a locked mutex
    pub fn free(mutex: &KMutexRef) {
        let mut mutex_table = MUTEX_TABLE.lock();

        let mutex = mutex_table.get_mut(&mutex.0).unwrap();

        // Unblock the next waiter if there is one
        if let Some(next_waiter) = mutex.waiters.pop_front() {
            with_scheduler(|scheduler| {
                scheduler.wake_up(next_waiter);
            });
        }
    }
}
