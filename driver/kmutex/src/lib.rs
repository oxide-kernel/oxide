#![no_std]
extern crate alloc;

pub mod driver;
pub mod interface;

use oxide_multitask::thread::ThreadId;
use oxide_multitask::with_scheduler;
